|SYNTHETIC SELFBOT|                                |REPORT BUGS|
-------------------->            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Supported-On  : Linux            | Instagram       : idice.security                |
Company       : iDiceSecurity    | Discord-Server  : https://discord.gg/cgfq5F9YYE |
Developer     : ReconEngine      | Discord         : 🅉🄴🅁🄾#6015                   |
HTML-BY       : Error            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-------------------->

# HOW TO RUN THE SELFBOT ?
-------> [python3 selfbot.py]

# HOW TO SETUP THE SELFBOT ?
-------> [cd INSTALL && pip install -r requirements.txt && cd ..]

# WHERE TO ADD TOKEN ? 
-------> [At the very bottom of the selfbot.py file]

# HOW TO INSPECT THE CODE ?
-------> [Use a texteditor]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
NOTE: IF RUNNING A [Linux] OPERATING SYSTEM, IT IS SAFE TO USE [sudo]
                                                                    |
# EXAMPLE                                                           |
-------> [sudo python3 selfbot.py]                                  |
-------> [sudo pip install -r requirements.txt]                     |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# PROBLEM WITH MODULE [functools]? TRY RUNNING THE FOLLOWING COMMAND
-------> [pip3 install functools]

NOTE: [pip] and [sudo] is accepted aswell


